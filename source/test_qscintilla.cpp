#include <memory>

#include <Qsci/qsciscintilla.h>

#include <tut/tut.hpp>

namespace tut
{
struct qscintilladata
{
};

typedef test_group<qscintilladata> tg;
tg qscintilla_test_group("Test import of QScintilla library.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("QScintilla: Test of creating an editor");

	const auto editor = std::make_unique<QsciScintilla>();
	ensure(editor.get() != nullptr);
}
} // namespace tut
