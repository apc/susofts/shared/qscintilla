[![pipeline status](https://gitlab.cern.ch/apc/susofts/shared/qscintilla/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/shared/qscintilla/commits/master)

QScintilla
=========

Clone of QScintilla that provides easy access to binaries.

Current available version is:
- QScintilla 2.11.1
- compiled against
	- MSVC 2017
	- G++ 8.2.1
- with Qt 5.12.2
- with C++17 features enabled

#### Table of Content ####

[Purpose](#purpose)
- [How to use](#how-to-use)
- [Update QScintilla](#update-qscintilla)

[Download](#download)

[Documentation](#documentation)
- [User guide](#user-guide)
- [Other](#other)

[Build instructions](#build-instructions)
- [Requirements](#requirements)
- [Generate project](#generate-project)
- [Build](#build)
- [Tests](#tests)

Purpose
-------

QScintilla doesn't provide binaries. We use this project to ease the process of building the library. As the library is built on the same runners as the rest of our projects, we ensure binary compatibility between our binaries.

### How to use ###

The current project is a CMake project that will automatically fetch QScintilla, download and build it. Then CMake includes a very dumb test project that will just test that it is possible to link against the built library.

Thus, you have clues on how to integrate QScintilla in your CMake projects.

### Update QScintilla ###

To update the version of QScintilla built, you just need to change the file [source/QScintilla/CMakeLists.txt](./source/QScintilla/CMakeLists.txt) and provide a link to the last version. See the [URL line](./source/QScintilla/CMakeLists.txt#L52).

It is always better to download the artifacts afterwards and compute their checksum, and then update this file. CMake works with checksum so this ease the process.

On Windows or Linux, you can run:

```bash
 sha512sum lib*.zip
```

Once this is done, you can update the link to download and the SHA inside the SUGL to use the last version. See https://gitlab.cern.ch/apc/susofts/shared/SUGL/-/tree/master/source/QScintilla for more information.

Download
--------

You can download the last version of QScintilla here:
- Linux binaries:
    - [liblinux.zip](https://gitlab.cern.ch/apc/susofts/shared/qscintilla/-/jobs/30655673/artifacts/download)
    - sha512: `4467ddaee52a9c4a36b0fd22d0fe2ccc94109b9d3882a819e0a61d7d081ae0edb09e6820bd2cbe359813a4b3481d4ae5daff23add8c26fbef1631d34ef05bc0b`
- Windows bibaries:
    - [libwin64.zip](https://gitlab.cern.ch/apc/susofts/shared/qscintilla/-/jobs/30655674/artifacts/download)
    - sha512: `67d33c39089ac282c65a3cdd1b9e8872dd4d8c61f3ddf19bae931d6a567cd36f3f4669882eea29cdb202ad14f78ce1e4586b5ba15aa5bc43244df3da505c8a72`

Documentation
-------------

### User guide ###

You can find all the user documentation here:
- [Scintilla](https://www.scintilla.org/ScintillaDoc.html)
- [QScintilla](https://www.riverbankcomputing.com/static/Docs/QScintilla/)

### Other ###

NYA

Build instructions
------------------

Before starting, you can have a look at the documentation about [Getting started with C++](https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22153013) for the CERN survey applications.

### Requirements ###

QScintilla can be built on Windows or Linux. To do so, you need at least:
- a C++17 compiler
- CMake 3.10+
- TUT (for the test only)
- Qt

For Windows, you can follow the steps in the aforementioned [Getting started with C++](https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22153013) documentation.

For Linux, you have an example of the needed steps in the [Dockerfile](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp/blob/master/Dockerfile) of the [sus_ci_cppworker](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp) project (the Docker image used to automatically run the tests on GitLab-CI).
Note that the `devtoolset` trick is only necessary on the CC7 (Cern CentOS 7) as it doesn't provide a C++17 compiler by default.

### Generate project ###

For compatibility reasons, we use CMake to generate projects, thus it is possible to generate projects for MSVC, Eclipse, or simple Unix makefiles. See the [CMake Generators documentation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) page.

Though, QScintilla itself it a QMake project, so you strictly need only QMake to build it...

To generate the project, you need first to create a subdirectory named `build/`, and then run CMake inside:

```bash
$ mkdir build && cd build/
$ cmake -G "Visual Studio 16 2019" -A x64 ../source # Use another generator here if you wish
```

### Build ###

Once generated, you can open your project in the `build/` subfolder. If you use MSVC, you can open the file `build/QScintilla_tests.sln`.

you can see that CMake has generated several targets, among others:
- `ALL_BUILD` builds all except the doxygen documentation
- `ZERO_CHECK` reruns CMake and automatically updates your project
- `ExternalProjectTargets`
	- `QScintilla` Download QScintilla
	- `QScintilla-qmake-build` build the library
- `QScintilla_tests` builds a test executable (default project in MSVC)

### Tests ###

The implemented test is very simple: it just tests that the compilation against the library is possible, so everything is correctly configured.
